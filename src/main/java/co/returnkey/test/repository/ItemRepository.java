package co.returnkey.test.repository;

import co.returnkey.test.entity.Item;
import co.returnkey.test.entity.ItemID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends JpaRepository<Item, ItemID> {
}
