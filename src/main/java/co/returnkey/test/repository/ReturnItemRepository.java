package co.returnkey.test.repository;

import co.returnkey.test.entity.ReturnItem;
import co.returnkey.test.entity.ReturnItemID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReturnItemRepository extends JpaRepository<ReturnItem, ReturnItemID> {

}
