package co.returnkey.test.repository;

import co.returnkey.test.entity.Return;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReturnRepository extends JpaRepository<Return, String> {
	List<Return> findAllByOrderId(String orderId);
}
