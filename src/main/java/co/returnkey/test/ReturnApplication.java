package co.returnkey.test;

import co.returnkey.test.entity.Item;
import co.returnkey.test.entity.Order;
import co.returnkey.test.service.OrderService;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.*;

@SpringBootApplication
public class ReturnApplication {
	private static final String COMMA_DELIMITER = ",";

	public static void main(String[] args) throws FileNotFoundException {
		ApplicationContext applicationContext = SpringApplication.run(ReturnApplication.class, args);
		OrderService service = applicationContext.getBean(OrderService.class);
		initOrder(service);
	}

	public static void initOrder(OrderService orderService) throws FileNotFoundException {
		Map<String, Order> orderMap = new HashMap<>();
		try (Scanner scanner = new Scanner(new File("orders.csv"))) {
			// skip 1st line
			scanner.nextLine();
			while (scanner.hasNextLine()) {
				try (Scanner rowScanner = new Scanner(scanner.nextLine())) {
					rowScanner.useDelimiter(COMMA_DELIMITER);
					int i = 0;
					String orderId = null, emailAddress = null, sku = null, quantity = null, price = null, itemName = null;
					while (rowScanner.hasNext()) {
						switch (i) {
							case 0:
								orderId = rowScanner.next();
								break;
							case 1:
								emailAddress = rowScanner.next();
								break;
							case 2:
								sku = rowScanner.next();
								break;
							case 3:
								quantity = rowScanner.next();
								break;
							case 4:
								price = rowScanner.next();
								break;
							case 5:
								itemName = rowScanner.next();
								break;
						}
						i++;
					}
					if (!StringUtils.hasText(orderId)) {
						continue;
					}

					Order order = new Order(orderId, emailAddress);
					Item item = new Item(order, sku, itemName, Integer.parseInt(quantity), new BigDecimal(price));

					if (orderMap.containsKey(orderId)) {
						orderMap.get(orderId).addItem(item);
						continue;
					}

					order.setItems(new ArrayList<>(Collections.singleton(item)));
					orderMap.put(orderId, order);

				}
			}
		}

		orderMap.forEach((orderId, order) -> {
			orderService.create(order);
		});
	}

	@Bean
	public ObjectMapper objectMapper(Jackson2ObjectMapperBuilder mapperBuilder) {
		return mapperBuilder.build().setSerializationInclusion(JsonInclude.Include.NON_NULL);
	}
}
