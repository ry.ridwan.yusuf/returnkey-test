package co.returnkey.test.service;

import co.returnkey.test.entity.Item;
import co.returnkey.test.entity.Order;
import co.returnkey.test.repository.ItemRepository;
import co.returnkey.test.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class OrderService {
	private OrderRepository repository;
	private ItemRepository itemRepository;

	@Autowired
	public OrderService(OrderRepository repository, ItemRepository itemRepository) {
		this.repository = repository;
		this.itemRepository = itemRepository;
	}

	public void create(Order order) {
		List<Item> items = order.getItems();
		order.setItems(null);
		repository.save(order);
		for (Item item : items) {
			itemRepository.save(item);
		}
	}

	public Optional<Order> find(String orderId) {
		return repository.findById(orderId);
	}

	public List<Order> getOrders() {
		return repository.findAll();
	}

	public Map<String, Item> getItemsAsMap(String orderId) {
		Order order = find(orderId).orElseThrow(() -> new ResponseStatusException(
				HttpStatus.INTERNAL_SERVER_ERROR, "Order not found"));
		Map<String, Item> result = new HashMap<>();
		order.getItems().forEach(item -> result.put(item.getId().sku, item));
		return result;
	}
}
