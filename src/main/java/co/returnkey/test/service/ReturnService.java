package co.returnkey.test.service;

import co.returnkey.test.entity.Return;
import co.returnkey.test.entity.ReturnItem;
import co.returnkey.test.enums.ReturnItemStatus;
import co.returnkey.test.repository.ReturnItemRepository;
import co.returnkey.test.repository.ReturnRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ReturnService {
	private ReturnRepository repository;
	private ReturnItemRepository itemRepository;

	@Autowired
	public ReturnService(ReturnRepository repository, ReturnItemRepository itemRepository) {
		this.repository = repository;
		this.itemRepository = itemRepository;
	}

	public void create(Return return_) {
		List<ReturnItem> items = return_.getItems();
		return_.setItems(null);
		repository.save(return_);
		for (ReturnItem item : items) {
			itemRepository.save(item);
		}
	}

	public void save(Return return_) {
		repository.save(return_);
	}

	public List<Return> findAllByOrderById(String orderId) {
		return repository.findAllByOrderId(orderId);
	}

	public Optional<Return> findById(String id) {
		return repository.findById(id);
	}

	public void updateQCStatus(ReturnItem returnItem, ReturnItemStatus status) {
		returnItem.setStatus(status);
		itemRepository.save(returnItem);
	}
}
