package co.returnkey.test.service;

import co.returnkey.test.entity.Token;
import co.returnkey.test.repository.TokenRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class TokenService {

	private TokenRepository repository;
	private ObjectMapper mapper;

	@Autowired
	public TokenService(TokenRepository repository) {
		this.repository = repository;
		this.mapper = new ObjectMapper();
	}

	public String generateToken(Object value) {
		Token token = null;
		try {
			token = repository.save(new Token(mapper.writeValueAsString(value)));
			return token.getToken();
		} catch (JsonProcessingException e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error reading token value");
		}
	}

	public <T> T getTokenValue(String token, Class<T> klass) {
		try {
			Token tokenObject = repository.findById(token).orElseThrow(() -> {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Token not found");
			});
			if (tokenObject.isUsed()) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Token already used");
			}
			tokenObject.setUsed(true);
			repository.save(tokenObject);
			return mapper.readValue(tokenObject.getValue(), klass);
		} catch (JsonProcessingException e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error reading token value");
		}
	}
}
