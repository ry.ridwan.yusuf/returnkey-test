package co.returnkey.test.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

public record ItemRequest(@NotEmpty String sku, @Min(1) int returnQuantity) {
}
