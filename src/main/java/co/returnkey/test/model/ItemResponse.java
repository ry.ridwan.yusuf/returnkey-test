package co.returnkey.test.model;

import co.returnkey.test.enums.ReturnItemStatus;

public record ItemResponse(String sku, int quantity, int returnQuantity, ReturnItemStatus status) {
}
