package co.returnkey.test.model;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

public record CreateReturnRequest(@NotEmpty String token, @NotEmpty List<@Valid ItemRequest> items) {
}
