package co.returnkey.test.model;

import javax.validation.constraints.NotEmpty;

public record PendingReturnRequest(@NotEmpty String orderId, @NotEmpty String email) {
}
