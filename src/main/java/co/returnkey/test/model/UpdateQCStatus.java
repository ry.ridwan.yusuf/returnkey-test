package co.returnkey.test.model;

import co.returnkey.test.enums.ReturnItemStatus;

import javax.validation.constraints.NotNull;

public record UpdateQCStatus(@NotNull ReturnItemStatus status) {
}
