package co.returnkey.test.model;

import co.returnkey.test.enums.ReturnStatus;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public record ReturnResponse(String id, String orderId, ReturnStatus status, String estimatedAmount,
							 String refundAmount, List<ItemResponse> items) {
}
