package co.returnkey.test.model;

public record PendingReturnResponse(String token) {
}
