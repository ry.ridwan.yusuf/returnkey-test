package co.returnkey.test.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.List;

@Entity(name = "orders")
public class Order implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;
	@Column(nullable = false)
	private String email;
	@OneToMany(mappedBy = "id.order")
	private List<Item> items;

	public Order() {
	}

	public Order(String id, String email) {
		this.id = id;
		this.email = email;
	}

	public void addItem(Item item) {
		this.items.add(item);
	}

	public String getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}
}
