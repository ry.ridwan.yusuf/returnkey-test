package co.returnkey.test.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import java.io.Serializable;
import java.util.UUID;

@Entity(name = "tokens")
public class Token implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String token;

	@Column(nullable = false, name = "value_object")
	@Lob
	private String value;
	@Column
	private boolean used;

	public Token() {
	}

	public Token(String value) {
		this.token = UUID.randomUUID().toString().replace("-", "");
		this.value = value;
		this.used = false;
	}

	public String getToken() {
		return token;
	}

	public String getValue() {
		return value;
	}

	public boolean isUsed() {
		return used;
	}

	public void setUsed(boolean used) {
		this.used = used;
	}
}
