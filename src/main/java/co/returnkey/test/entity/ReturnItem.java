package co.returnkey.test.entity;

import co.returnkey.test.enums.ReturnItemStatus;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
public class ReturnItem implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ReturnItemID id;

	@Column(nullable = false)
	private int quantity;
	@Column(nullable = false)
	private int returnQuantity;
	@Column(nullable = false)
	private BigDecimal price;

	@Enumerated(EnumType.STRING)
	@Column
	private ReturnItemStatus status;

	public ReturnItem() {
	}

	public ReturnItem(Return return_, String sku, int quantity, int returnQuantity, BigDecimal price,
			ReturnItemStatus status) {
		this.id = new ReturnItemID(return_, sku);
		this.quantity = quantity;
		this.status = status;
		this.returnQuantity = returnQuantity;
		this.price = price;
	}

	public ReturnItem(Return return_, String sku, int quantity, int returnQuantity, BigDecimal price) {
		this(return_, sku, quantity, returnQuantity, price, ReturnItemStatus.PENDING);
	}

	public ReturnItemID getId() {
		return id;
	}

	public int getQuantity() {
		return quantity;
	}

	public int getReturnQuantity() {
		return returnQuantity;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public ReturnItemStatus getStatus() {
		return status;
	}

	public void setStatus(ReturnItemStatus status) {
		this.status = status;
	}
}
