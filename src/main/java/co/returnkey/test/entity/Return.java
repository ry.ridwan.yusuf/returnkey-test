package co.returnkey.test.entity;

import co.returnkey.test.enums.ReturnStatus;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Entity(name = "returns")
public class Return implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;
	@Column
	@JoinColumn(name = "order_id")
	private String orderId;

	@OneToMany(mappedBy = "id.return_")
	private List<ReturnItem> items;
	@Enumerated(EnumType.STRING)
	@Column
	private ReturnStatus status;

	public Return() {
	}

	public Return(String orderId, ReturnStatus status) {
		this.id = UUID.randomUUID().toString().replace("-", "");
		this.orderId = orderId;
		this.status = status;
	}

	public Return(String orderId) {
		this(orderId, ReturnStatus.AWAITING_APPROVAL);
	}

	public String getId() {
		return id;
	}

	public String getOrderId() {
		return orderId;
	}

	public List<ReturnItem> getItems() {
		return items;
	}

	public void setItems(List<ReturnItem> items) {
		this.items = items;
	}

	public ReturnStatus getStatus() {
		return status;
	}

	public void setStatus(ReturnStatus status) {
		this.status = status;
	}
}
