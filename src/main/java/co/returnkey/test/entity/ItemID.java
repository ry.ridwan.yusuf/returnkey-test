package co.returnkey.test.entity;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class ItemID implements Serializable {
	private static final long serialVersionUID = 1L;
	//	public String orderId;
	public String sku;
	@ManyToOne
	@JoinColumn(name = "order_id")
	private Order order;

	public ItemID() {
	}

	public ItemID(Order order, String sku) {
		this.order = order;
		this.sku = sku;
	}
}
