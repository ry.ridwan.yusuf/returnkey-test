package co.returnkey.test.entity;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class ReturnItemID implements Serializable {
	private static final long serialVersionUID = 1L;
	private String sku;
	@ManyToOne
	@JoinColumn(name = "return_id")
	private Return return_;

	public ReturnItemID() {
	}

	public ReturnItemID(Return return_, String sku) {
		this.return_ = return_;
		this.sku = sku;
	}

	public String getSku() {
		return sku;
	}
}
