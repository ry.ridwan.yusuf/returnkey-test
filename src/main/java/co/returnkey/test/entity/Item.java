package co.returnkey.test.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
public class Item implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ItemID id;

	@Column(nullable = false)
	private String name;
	@Column(nullable = false)
	private int quantity;
	// price per item count
	@Column(nullable = false)
	private BigDecimal price;

	public Item() {
	}

	public Item(Order order, String sku, String name, int quantity, BigDecimal price) {
		this.id = new ItemID(order, sku);
		this.name = name;
		this.quantity = quantity;
		this.price = price;
	}

	public ItemID getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getQuantity() {
		return quantity;
	}

	public BigDecimal getPrice() {
		return price;
	}
}
