package co.returnkey.test.enums;

public enum ReturnStatus {
	AWAITING_APPROVAL,
	COMPLETE
}
