package co.returnkey.test.enums;

public enum ReturnItemStatus {
	PENDING,
	ACCEPTED,
	REJECTED
}
