package co.returnkey.test.controller;

import co.returnkey.test.entity.Order;
import co.returnkey.test.model.PendingReturnRequest;
import co.returnkey.test.model.PendingReturnResponse;
import co.returnkey.test.service.OrderService;
import co.returnkey.test.service.ReturnService;
import co.returnkey.test.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/pending/returns")
public class PendingReturnController {

	private ReturnService returnService;
	private OrderService orderService;
	private TokenService tokenService;

	@Autowired
	public PendingReturnController(ReturnService returnService, OrderService orderService, TokenService tokenService) {
		this.returnService = returnService;
		this.orderService = orderService;
		this.tokenService = tokenService;
	}

	@PostMapping
	public PendingReturnResponse pendingReturns(@RequestBody @Valid PendingReturnRequest request) {
		//validate exist in order table
		Optional<Order> order = orderService.find(request.orderId());
		if (order.isEmpty() || !order.get().getEmail().equals(request.email())) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Order not found");
		}
		String token = tokenService.generateToken(request);
		return new PendingReturnResponse(token);
	}
}