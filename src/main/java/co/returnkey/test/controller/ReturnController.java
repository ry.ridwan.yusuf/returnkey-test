package co.returnkey.test.controller;

import co.returnkey.test.entity.Item;
import co.returnkey.test.entity.Return;
import co.returnkey.test.entity.ReturnItem;
import co.returnkey.test.enums.ReturnItemStatus;
import co.returnkey.test.enums.ReturnStatus;
import co.returnkey.test.model.*;
import co.returnkey.test.service.OrderService;
import co.returnkey.test.service.ReturnService;
import co.returnkey.test.service.TokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

@RestController
@RequestMapping("/returns")
public class ReturnController {
	private static final Logger LOG = LoggerFactory.getLogger(ReturnController.class);
	private static final Set<ReturnItemStatus> ALLOWED_RETURN_ITEM_STATUS_UPDATE = Set.of(ReturnItemStatus.ACCEPTED,
			ReturnItemStatus.REJECTED);
	private ReturnService returnService;
	private OrderService orderService;
	private TokenService tokenService;

	@Autowired
	public ReturnController(ReturnService returnService, OrderService orderService, TokenService tokenService) {
		this.returnService = returnService;
		this.orderService = orderService;
		this.tokenService = tokenService;
	}

	private Set<String> validateCreateRequestAndGetSkuRequest(CreateReturnRequest request) {
		if (request.items().isEmpty()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No items found in request");
		}
		Set<String> skuRequest = new HashSet<>();
		for (ItemRequest itemRequest : request.items()) {
			if (skuRequest.contains(itemRequest.sku())) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Multiple identical item found in request");
			}
			skuRequest.add(itemRequest.sku());
		}
		return skuRequest;
	}

	private void validateReturnItem(CreateReturnRequest request, String orderId, Set<String> skuRequest,
			Map<String, Item> orderItemMap) {

		List<Return> returns = returnService.findAllByOrderById(orderId);
		if (returns.size() > 0) {
			for (Return return_ : returns) {
				for (ReturnItem returnItem : return_.getItems()) {
					if (skuRequest.contains(returnItem.getId().getSku())) {
						throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Cannot return item twice");
					}
				}
			}
		}

		for (ItemRequest item : request.items()) {
			Item orderItem = orderItemMap.get(item.sku());
			if (null == orderItem) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Sku not found");
			}

			if (orderItem.getQuantity() < item.returnQuantity()) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Return quantity is greater than quantity");
			}
		}
	}

	@PostMapping
	public ReturnResponse create(@RequestBody CreateReturnRequest request) {
		//validate request
		Set<String> skuRequest = validateCreateRequestAndGetSkuRequest(request);
		//validate token
		PendingReturnRequest tokenValue = tokenService.getTokenValue(request.token(), PendingReturnRequest.class);
		//validate return
		Map<String, Item> orderItemMap = orderService.getItemsAsMap(tokenValue.orderId());
		validateReturnItem(request, tokenValue.orderId(), skuRequest, orderItemMap);

		//create return
		Return return_ = new Return(tokenValue.orderId());
		List<ReturnItem> returnItems = new ArrayList<>();
		List<ItemResponse> returnItemsResponse = new ArrayList<>();
		BigDecimal estimatedAmount = BigDecimal.ZERO;
		for (ItemRequest item : request.items()) {
			Item orderItem = orderItemMap.get(item.sku());
			estimatedAmount = estimatedAmount.add(orderItem.getPrice().multiply(new BigDecimal(item.returnQuantity())));
			returnItems.add(new ReturnItem(return_, item.sku(), orderItem.getQuantity(), item.returnQuantity(),
					orderItem.getPrice()));
			returnItemsResponse.add(new ItemResponse(item.sku(), orderItem.getQuantity(), item.returnQuantity(),
					ReturnItemStatus.PENDING));
		}
		return_.setItems(returnItems);
		returnService.create(return_);

		return new ReturnResponse(return_.getId(), return_.getOrderId(), return_.getStatus(),
				estimatedAmount.setScale(2, RoundingMode.HALF_UP).toString(), null,
				returnItemsResponse);
	}

	@GetMapping("/{id}")
	public ReturnResponse getReturn(@PathVariable String id) {
		Return return_ = returnService.findById(id).orElseThrow(() -> {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Return not found");
		});
		List<ItemResponse> returnItemsResponse = new ArrayList<>();
		BigDecimal amount = BigDecimal.ZERO;
		for (ReturnItem item : return_.getItems()) {
			if (ReturnItemStatus.REJECTED != item.getStatus()) {
				amount = amount.add(item.getPrice().multiply(new BigDecimal(item.getReturnQuantity())));
			}
			returnItemsResponse.add(
					new ItemResponse(item.getId().getSku(), item.getQuantity(), item.getReturnQuantity(),
							item.getStatus()));
		}
		String estimatedAmount = null;
		String refundAmount = null;
		if (ReturnStatus.AWAITING_APPROVAL == return_.getStatus()) {
			estimatedAmount = amount.setScale(2, RoundingMode.HALF_UP).toString();
		} else {
			refundAmount = amount.setScale(2, RoundingMode.HALF_UP).toString();
		}

		return new ReturnResponse(return_.getId(), return_.getOrderId(), return_.getStatus(),
				estimatedAmount, refundAmount,
				returnItemsResponse);
	}

	@PutMapping("/{id}/items/{sku}/qc/status")
	public void updateQCStatus(@PathVariable String id, @PathVariable String sku,
			@Valid @RequestBody UpdateQCStatus status) {
		if (!ALLOWED_RETURN_ITEM_STATUS_UPDATE.contains(status.status())) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid qc status");
		}
		Return return_ = returnService.findById(id).orElseThrow(() -> {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Return not found");
		});

		ReturnItem returnItem = return_.getItems().stream().filter(entry -> entry.getId().getSku().equals(sku))
				.findFirst()
				.orElseThrow(() -> {
					throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Return item not found");
				});

		returnService.updateQCStatus(returnItem, status.status());
		if (return_.getItems().stream().filter(entry -> entry.getStatus() == ReturnItemStatus.PENDING)
				.findFirst()
				.isEmpty()) {
			return_.setStatus(ReturnStatus.COMPLETE);
			returnService.save(return_);
		}

	}

}
