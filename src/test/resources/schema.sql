-- H2 2.1.214; 
;              
CREATE USER IF NOT EXISTS "SA" SALT '83908f45c1bf3ef2' HASH 'cc9cf9eb08dc60b816f493ecfcc349d03ef598d005dc9ccb1df58ac1b107d7f6' ADMIN;          
CREATE CACHED TABLE "PUBLIC"."ITEM"(
    "SKU" CHARACTER VARYING(255) NOT NULL,
    "NAME" CHARACTER VARYING(255) NOT NULL,
    "PRICE" NUMERIC(19, 2) NOT NULL,
    "QUANTITY" INTEGER NOT NULL,
    "ORDER_ID" CHARACTER VARYING(255) NOT NULL
);            
ALTER TABLE "PUBLIC"."ITEM" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_2" PRIMARY KEY("ORDER_ID", "SKU");             

CREATE CACHED TABLE "PUBLIC"."ORDERS"(
    "ID" CHARACTER VARYING(255) NOT NULL,
    "EMAIL" CHARACTER VARYING(255) NOT NULL
);
ALTER TABLE "PUBLIC"."ORDERS" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_8" PRIMARY KEY("ID");        

CREATE CACHED TABLE "PUBLIC"."RETURN_ITEM"(
    "SKU" CHARACTER VARYING(255) NOT NULL,
    "PRICE" NUMERIC(19, 2) NOT NULL,
    "QUANTITY" INTEGER NOT NULL,
    "RETURN_QUANTITY" INTEGER NOT NULL,
    "STATUS" CHARACTER VARYING(255),
    "RETURN_ID" CHARACTER VARYING(255) NOT NULL
);   
ALTER TABLE "PUBLIC"."RETURN_ITEM" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_29" PRIMARY KEY("RETURN_ID", "SKU");    
-- 0 +/- SELECT COUNT(*) FROM PUBLIC.RETURN_ITEM;              
CREATE CACHED TABLE "PUBLIC"."RETURNS"(
    "ID" CHARACTER VARYING(255) NOT NULL,
    "ORDER_ID" CHARACTER VARYING(255),
    "STATUS" CHARACTER VARYING(255)
);
ALTER TABLE "PUBLIC"."RETURNS" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_6" PRIMARY KEY("ID");       
-- 0 +/- SELECT COUNT(*) FROM PUBLIC.RETURNS;  
CREATE CACHED TABLE "PUBLIC"."TOKENS"(
    "TOKEN" CHARACTER VARYING(255) NOT NULL,
    "VALUE_OBJECT" CHARACTER LARGE OBJECT NOT NULL
);      
ALTER TABLE "PUBLIC"."TOKENS" ADD CONSTRAINT "PUBLIC"."CONSTRAINT_9" PRIMARY KEY("TOKEN");     
-- 0 +/- SELECT COUNT(*) FROM PUBLIC.TOKENS;   
ALTER TABLE "PUBLIC"."ITEM" ADD CONSTRAINT "PUBLIC"."FKTBIPB7UOB2UD46FP9QCN6JL86" FOREIGN KEY("ORDER_ID") REFERENCES "PUBLIC"."ORDERS"("ID") NOCHECK;          
ALTER TABLE "PUBLIC"."RETURN_ITEM" ADD CONSTRAINT "PUBLIC"."FK9IT3JIE1JXH9N7XUXI9H7S0JJ" FOREIGN KEY("RETURN_ID") REFERENCES "PUBLIC"."RETURNS"("ID") NOCHECK; 
