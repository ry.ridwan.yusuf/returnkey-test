INSERT INTO "PUBLIC"."ORDERS" VALUES
('RK-238', 'carly@example.com'),
('RK-149', 'dalton@example.com'),
('RK-478', 'john@example.com'),
('RK-642', 'will@example.com'),
('RK-912', 'karen@example.com'),
('RK-239', 'steve@example.com');


INSERT INTO "PUBLIC"."ITEM" VALUES
('MU-5091', 'Lip Gloss', 15.25, 3, 'RK-238'),
('NIKE-56', 'Nike Medium Red Leggings', 75.50, 1, 'RK-238'),
('MU-4129', 'Eye Shadow', 22.85, 2, 'RK-238'),
('SOC-1', 'Soccer Ball', 25.99, 1, 'RK-149'),
('NIKE-143', 'Nike Mercurial Superfly 8 Elite FG Firm Ground Soccer Cleat', 249.99, 1, 'RK-149'),
('MENS-156', 'Small Black T-Shirt', 50.00, 2, 'RK-478'),
('NIKE-7', 'Nike Air Jordans - Size 7', 110.75, 1, 'RK-478'),
('SOC-1', 'Soccer Ball', 25.99, 2, 'RK-642'),
('PAR-14', U&'Chanel - CHANCE EAU FRA\00ceCHE Eau de Toilette', 149.99, 1, 'RK-912'),
('MENS-156', 'Small Black T-Shirt', 50.00, 1, 'RK-239');

