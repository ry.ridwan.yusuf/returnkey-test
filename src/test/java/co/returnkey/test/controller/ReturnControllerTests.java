package co.returnkey.test.controller;

import co.returnkey.test.enums.ReturnItemStatus;
import co.returnkey.test.enums.ReturnStatus;
import co.returnkey.test.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class ReturnControllerTests {
	@Autowired
	private MockMvc mvc;
	private ObjectMapper mapper = new ObjectMapper();

	@Test
	public void givenValidRequest_whenPostReturn_thenReturnSuccess() throws Exception {
		PendingReturnRequest request = new PendingReturnRequest("RK-238", "carly@example.com");

		MvcResult pendingReturnResult = mvc.perform(post("/pending/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(request))
				).andExpect(status().isOk())
				.andExpect(jsonPath("token", isA(String.class)))
				.andReturn();

		PendingReturnResponse response =
				mapper.readValue(pendingReturnResult.getResponse().getContentAsString(), PendingReturnResponse.class);
		List<ItemRequest> itemRequests = new ArrayList<>();
		itemRequests.add(new ItemRequest("MU-5091", 2));
		CreateReturnRequest createRequest = new CreateReturnRequest(response.token(), itemRequests);

		mvc.perform(post("/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(createRequest))
				).andExpect(status().isOk())
				.andExpect(jsonPath("id", isA(String.class)))
				.andExpect(jsonPath("orderId", is("RK-238")))
				.andExpect(jsonPath("status", is(ReturnStatus.AWAITING_APPROVAL.name())))
				.andExpect(jsonPath("estimatedAmount", is("30.50")))
				.andExpect(jsonPath("$", not(hasProperty("refundAmount"))))
				.andExpect(jsonPath("items", hasSize(1)))
				.andExpect(jsonPath("items[0].sku", is("MU-5091")))
		;
	}

	@Test
	public void givenUsedToken_whenPostReturn_thenReturnInvalidToken() throws Exception {
		PendingReturnRequest request = new PendingReturnRequest("RK-238", "carly@example.com");

		MvcResult result = mvc.perform(post("/pending/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(request))
				).andExpect(status().isOk())
				.andExpect(jsonPath("token", isA(String.class)))
				.andReturn();

		PendingReturnResponse response =
				mapper.readValue(result.getResponse().getContentAsString(), PendingReturnResponse.class);
		List<ItemRequest> itemRequests = new ArrayList<>();
		itemRequests.add(new ItemRequest("MU-5091", 2));
		CreateReturnRequest createRequest = new CreateReturnRequest(response.token(), itemRequests);

		mvc.perform(post("/returns")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(createRequest))
		).andExpect(status().isOk());

		mvc.perform(post("/returns")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(createRequest))
		).andExpect(status().isNotFound());
	}

	@Test
	public void givenEmptyItemList_whenPostReturn_thenReturnBadRequest() throws Exception {
		PendingReturnRequest request = new PendingReturnRequest("RK-238", "carly@example.com");

		MvcResult result = mvc.perform(post("/pending/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(request))
				).andExpect(status().isOk())
				.andExpect(jsonPath("token", isA(String.class)))
				.andReturn();

		PendingReturnResponse response =
				mapper.readValue(result.getResponse().getContentAsString(), PendingReturnResponse.class);
		List<ItemRequest> itemRequests = new ArrayList<>();
		CreateReturnRequest createRequest = new CreateReturnRequest(response.token(), itemRequests);

		mvc.perform(post("/returns")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(createRequest))
		).andExpect(status().isBadRequest());
	}

	@Test
	public void givenInvalidToken_whenPostReturn_thenReturnNotFound() throws Exception {

		List<ItemRequest> itemRequests = new ArrayList<>();
		itemRequests.add(new ItemRequest("MU-5091", 2));
		CreateReturnRequest createRequest = new CreateReturnRequest("INVALID TOKEN", itemRequests);

		mvc.perform(post("/returns")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(createRequest))
		).andExpect(status().isNotFound());
	}

	@Test
	public void givenReturnQuantityMoreThanQuantity_whenPostReturn_thenReturnBadRequest() throws Exception {
		PendingReturnRequest request = new PendingReturnRequest("RK-238", "carly@example.com");

		MvcResult pendingReturnResult = mvc.perform(post("/pending/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(request))
				).andExpect(status().isOk())
				.andExpect(jsonPath("token", isA(String.class)))
				.andReturn();

		PendingReturnResponse response =
				mapper.readValue(pendingReturnResult.getResponse().getContentAsString(), PendingReturnResponse.class);
		List<ItemRequest> itemRequests = new ArrayList<>();
		itemRequests.add(new ItemRequest("MU-5091", 4));
		CreateReturnRequest createRequest = new CreateReturnRequest(response.token(), itemRequests);

		mvc.perform(post("/returns")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(createRequest))
		).andExpect(status().isBadRequest());

	}

	@Test
	public void givenSkuNotFoundInOrder_whenPostReturn_thenReturnBadRequest() throws Exception {
		PendingReturnRequest request = new PendingReturnRequest("RK-238", "carly@example.com");

		MvcResult pendingReturnResult = mvc.perform(post("/pending/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(request))
				).andExpect(status().isOk())
				.andExpect(jsonPath("token", isA(String.class)))
				.andReturn();

		PendingReturnResponse response =
				mapper.readValue(pendingReturnResult.getResponse().getContentAsString(), PendingReturnResponse.class);
		List<ItemRequest> itemRequests = new ArrayList<>();
		itemRequests.add(new ItemRequest("RY-5091", 4));
		CreateReturnRequest createRequest = new CreateReturnRequest(response.token(), itemRequests);

		mvc.perform(post("/returns")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(createRequest))
		).andExpect(status().isBadRequest());

	}

	@Test
	public void givenSameOrderIdReturnedWithDifferentSku_whenPostReturnTwice_thenReturnSuccess() throws Exception {
		PendingReturnRequest request = new PendingReturnRequest("RK-238", "carly@example.com");

		MvcResult pendingReturnResult = mvc.perform(post("/pending/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(request))
				).andExpect(status().isOk())
				.andExpect(jsonPath("token", isA(String.class)))
				.andReturn();

		PendingReturnResponse response =
				mapper.readValue(pendingReturnResult.getResponse().getContentAsString(), PendingReturnResponse.class);
		List<ItemRequest> itemRequests = new ArrayList<>();
		itemRequests.add(new ItemRequest("MU-5091", 2));
		CreateReturnRequest createRequest = new CreateReturnRequest(response.token(), itemRequests);

		mvc.perform(post("/returns")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(createRequest))
		).andExpect(status().isOk());

		MvcResult pendingReturnResult2 = mvc.perform(post("/pending/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(request))
				).andExpect(status().isOk())
				.andExpect(jsonPath("token", isA(String.class)))
				.andReturn();

		PendingReturnResponse response2 =
				mapper.readValue(pendingReturnResult2.getResponse().getContentAsString(), PendingReturnResponse.class);
		List<ItemRequest> itemRequests2 = new ArrayList<>();
		itemRequests2.add(new ItemRequest("NIKE-56", 1));
		CreateReturnRequest createRequest2 = new CreateReturnRequest(response2.token(), itemRequests2);

		mvc.perform(post("/returns")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(createRequest2))
		).andExpect(status().isOk());

	}

	@Test
	public void givenSameOrderIdReturnedWithDifferentSku_whenPostReturnOnce_thenReturnSuccess() throws Exception {
		PendingReturnRequest request = new PendingReturnRequest("RK-238", "carly@example.com");

		MvcResult pendingReturnResult = mvc.perform(post("/pending/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(request))
				).andExpect(status().isOk())
				.andExpect(jsonPath("token", isA(String.class)))
				.andReturn();

		PendingReturnResponse response =
				mapper.readValue(pendingReturnResult.getResponse().getContentAsString(), PendingReturnResponse.class);
		List<ItemRequest> itemRequests = new ArrayList<>();
		itemRequests.add(new ItemRequest("MU-5091", 2));
		itemRequests.add(new ItemRequest("NIKE-56", 1));

		CreateReturnRequest createRequest = new CreateReturnRequest(response.token(), itemRequests);

		mvc.perform(post("/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(createRequest))
				).andExpect(status().isOk())
				.andExpect(jsonPath("id", isA(String.class)))
				.andExpect(jsonPath("orderId", is("RK-238")))
				.andExpect(jsonPath("status", is(ReturnStatus.AWAITING_APPROVAL.name())))
				.andExpect(jsonPath("estimatedAmount", is("106.00")))
				.andExpect(jsonPath("$", not(hasProperty("refundAmount"))))
				.andExpect(jsonPath("items", hasSize(2)))
				.andExpect(jsonPath("items[0].sku", is("MU-5091")))
		;
	}

	@Test
	public void givenSameOrderIdReturnedWithSameSku_whenPostReturnOnce_thenReturnSuccess() throws Exception {
		PendingReturnRequest request = new PendingReturnRequest("RK-238", "carly@example.com");

		MvcResult pendingReturnResult = mvc.perform(post("/pending/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(request))
				).andExpect(status().isOk())
				.andExpect(jsonPath("token", isA(String.class)))
				.andReturn();

		PendingReturnResponse response =
				mapper.readValue(pendingReturnResult.getResponse().getContentAsString(), PendingReturnResponse.class);
		List<ItemRequest> itemRequests = new ArrayList<>();
		itemRequests.add(new ItemRequest("MU-5091", 2));
		itemRequests.add(new ItemRequest("MU-5091", 1));

		CreateReturnRequest createRequest = new CreateReturnRequest(response.token(), itemRequests);

		mvc.perform(post("/returns")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(createRequest))
		).andExpect(status().isBadRequest())
		;
	}

	@Test
	public void givenSameOrderIdReturnedWithSameSku_whenPostReturnTwice_thenReturnBadRequest() throws Exception {
		PendingReturnRequest request = new PendingReturnRequest("RK-238", "carly@example.com");

		MvcResult pendingReturnResult = mvc.perform(post("/pending/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(request))
				).andExpect(status().isOk())
				.andExpect(jsonPath("token", isA(String.class)))
				.andReturn();

		PendingReturnResponse response =
				mapper.readValue(pendingReturnResult.getResponse().getContentAsString(), PendingReturnResponse.class);
		List<ItemRequest> itemRequests = new ArrayList<>();
		itemRequests.add(new ItemRequest("MU-5091", 2));
		CreateReturnRequest createRequest = new CreateReturnRequest(response.token(), itemRequests);

		mvc.perform(post("/returns")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(createRequest))
		).andExpect(status().isOk());

		MvcResult pendingReturnResult2 = mvc.perform(post("/pending/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(request))
				).andExpect(status().isOk())
				.andExpect(jsonPath("token", isA(String.class)))
				.andReturn();

		PendingReturnResponse response2 =
				mapper.readValue(pendingReturnResult2.getResponse().getContentAsString(), PendingReturnResponse.class);
		List<ItemRequest> itemRequests2 = new ArrayList<>();
		itemRequests2.add(new ItemRequest("MU-5091", 1));
		CreateReturnRequest createRequest2 = new CreateReturnRequest(response2.token(), itemRequests2);

		mvc.perform(post("/returns")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(createRequest2))
		).andExpect(status().isBadRequest());

	}

	@Test
	public void givenValidRequest_whenGetReturn_thenReturnSuccess() throws Exception {
		PendingReturnRequest request = new PendingReturnRequest("RK-238", "carly@example.com");

		MvcResult pendingReturnResult = mvc.perform(post("/pending/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(request))
				).andExpect(status().isOk())
				.andExpect(jsonPath("token", isA(String.class)))
				.andReturn();

		PendingReturnResponse response =
				mapper.readValue(pendingReturnResult.getResponse().getContentAsString(), PendingReturnResponse.class);
		List<ItemRequest> itemRequests = new ArrayList<>();
		itemRequests.add(new ItemRequest("MU-5091", 2));
		CreateReturnRequest createRequest = new CreateReturnRequest(response.token(), itemRequests);

		MvcResult createReturnResult = mvc.perform(post("/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(createRequest))
				).andExpect(status().isOk())
				.andReturn();

		ReturnResponse createReturnResponse =
				mapper.readValue(createReturnResult.getResponse().getContentAsString(), ReturnResponse.class);
		mvc.perform(get("/returns/" + createReturnResponse.id())
						.contentType(MediaType.APPLICATION_JSON)
				).andExpect(status().isOk())
				.andExpect(jsonPath("id", isA(String.class)))
				.andExpect(jsonPath("orderId", is("RK-238")))
				.andExpect(jsonPath("status", is(ReturnStatus.AWAITING_APPROVAL.name())))
				.andExpect(jsonPath("estimatedAmount", is("30.50")))
				.andExpect(jsonPath("$", not(hasProperty("refundAmount"))))
				.andExpect(jsonPath("items", hasSize(1)))
				.andExpect(jsonPath("items[0].sku", is("MU-5091")))
		;
	}

	@Test
	public void givenInvalidId_whenGetReturn_thenReturnNotFound() throws Exception {
		mvc.perform(get("/returns/" + "INVALID")
				.contentType(MediaType.APPLICATION_JSON)
		).andExpect(status().isNotFound());
	}

	@Test
	public void givenValidRequestAndAllItemAccepted_whenPutQCStatus_thenReturnSuccessAndReturnStatusComplete()
			throws Exception {
		PendingReturnRequest request = new PendingReturnRequest("RK-238", "carly@example.com");

		MvcResult pendingReturnResult = mvc.perform(post("/pending/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(request))
				).andExpect(status().isOk())
				.andExpect(jsonPath("token", isA(String.class)))
				.andReturn();

		PendingReturnResponse response =
				mapper.readValue(pendingReturnResult.getResponse().getContentAsString(), PendingReturnResponse.class);
		List<ItemRequest> itemRequests = new ArrayList<>();
		itemRequests.add(new ItemRequest("MU-5091", 2));
		CreateReturnRequest createRequest = new CreateReturnRequest(response.token(), itemRequests);

		MvcResult createReturnResult = mvc.perform(post("/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(createRequest))
				).andExpect(status().isOk())
				.andReturn();

		ReturnResponse createReturnResponse =
				mapper.readValue(createReturnResult.getResponse().getContentAsString(), ReturnResponse.class);

		mvc.perform(put("/returns/" + createReturnResponse.id() + "/items/" + "MU-5091" + "/qc/status")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(new UpdateQCStatus(ReturnItemStatus.ACCEPTED)))
		).andExpect(status().isOk());

		mvc.perform(get("/returns/" + createReturnResponse.id())
						.contentType(MediaType.APPLICATION_JSON)
				).andExpect(status().isOk())
				.andExpect(jsonPath("id", isA(String.class)))
				.andExpect(jsonPath("orderId", is("RK-238")))
				.andExpect(jsonPath("status", is(ReturnStatus.COMPLETE.name())))
				.andExpect(jsonPath("$", not(hasProperty("estimatedAmount"))))
				.andExpect(jsonPath("refundAmount", is("30.50")))
				.andExpect(jsonPath("items", hasSize(1)))
				.andExpect(jsonPath("items[0].sku", is("MU-5091")))
				.andExpect(jsonPath("items[0].status", is(ReturnItemStatus.ACCEPTED.name())))
		;
	}

	@Test
	public void givenValidRequestAndAllItemRejected_whenPutQCStatus_thenReturnSuccessAndReturnStatusCompleteAndRefundAmount0()
			throws Exception {
		PendingReturnRequest request = new PendingReturnRequest("RK-238", "carly@example.com");

		MvcResult pendingReturnResult = mvc.perform(post("/pending/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(request))
				).andExpect(status().isOk())
				.andExpect(jsonPath("token", isA(String.class)))
				.andReturn();

		PendingReturnResponse response =
				mapper.readValue(pendingReturnResult.getResponse().getContentAsString(), PendingReturnResponse.class);
		List<ItemRequest> itemRequests = new ArrayList<>();
		itemRequests.add(new ItemRequest("MU-5091", 2));
		CreateReturnRequest createRequest = new CreateReturnRequest(response.token(), itemRequests);

		MvcResult createReturnResult = mvc.perform(post("/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(createRequest))
				).andExpect(status().isOk())
				.andReturn();

		ReturnResponse createReturnResponse =
				mapper.readValue(createReturnResult.getResponse().getContentAsString(), ReturnResponse.class);

		mvc.perform(put("/returns/" + createReturnResponse.id() + "/items/" + "MU-5091" + "/qc/status")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(new UpdateQCStatus(ReturnItemStatus.REJECTED)))
		).andExpect(status().isOk());

		mvc.perform(get("/returns/" + createReturnResponse.id())
						.contentType(MediaType.APPLICATION_JSON)
				).andExpect(status().isOk())
				.andExpect(jsonPath("id", isA(String.class)))
				.andExpect(jsonPath("orderId", is("RK-238")))
				.andExpect(jsonPath("status", is(ReturnStatus.COMPLETE.name())))
				.andExpect(jsonPath("$", not(hasProperty("estimatedAmount"))))
				.andExpect(jsonPath("refundAmount", is("0.00")))
				.andExpect(jsonPath("items", hasSize(1)))
				.andExpect(jsonPath("items[0].sku", is("MU-5091")))
				.andExpect(jsonPath("items[0].status", is(ReturnItemStatus.REJECTED.name())))
		;
	}

	@Test
	public void givenValidRequestAndItemRejectedAndPending_whenPutQCStatus_thenReturnSuccessAndReturnStatusAwaitingApprovalAndEstimatedAmountPartial()
			throws Exception {
		PendingReturnRequest request = new PendingReturnRequest("RK-238", "carly@example.com");

		MvcResult pendingReturnResult = mvc.perform(post("/pending/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(request))
				).andExpect(status().isOk())
				.andExpect(jsonPath("token", isA(String.class)))
				.andReturn();

		PendingReturnResponse response =
				mapper.readValue(pendingReturnResult.getResponse().getContentAsString(), PendingReturnResponse.class);
		List<ItemRequest> itemRequests = new ArrayList<>();
		itemRequests.add(new ItemRequest("MU-5091", 2));
		itemRequests.add(new ItemRequest("NIKE-56", 1));
		CreateReturnRequest createRequest = new CreateReturnRequest(response.token(), itemRequests);

		MvcResult createReturnResult = mvc.perform(post("/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(createRequest))
				).andExpect(status().isOk())
				.andReturn();

		ReturnResponse createReturnResponse =
				mapper.readValue(createReturnResult.getResponse().getContentAsString(), ReturnResponse.class);

		mvc.perform(put("/returns/" + createReturnResponse.id() + "/items/" + "MU-5091" + "/qc/status")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(new UpdateQCStatus(ReturnItemStatus.REJECTED)))
		).andExpect(status().isOk());

		mvc.perform(get("/returns/" + createReturnResponse.id())
						.contentType(MediaType.APPLICATION_JSON)
				).andExpect(status().isOk())
				.andExpect(jsonPath("id", isA(String.class)))
				.andExpect(jsonPath("orderId", is("RK-238")))
				.andExpect(jsonPath("status", is(ReturnStatus.AWAITING_APPROVAL.name())))
				.andExpect(jsonPath("estimatedAmount", is("75.50")))
				.andExpect(jsonPath("$", not(hasProperty("refundAmount"))))
				.andExpect(jsonPath("items", hasSize(2)))
				.andExpect(jsonPath("items[0].sku", is("MU-5091")))
				.andExpect(jsonPath("items[0].status", is(ReturnItemStatus.REJECTED.name())))
		;
	}

	@Test
	public void givenValidRequestAndItemAcceptedAndPending_whenPutQCStatus_thenReturnSuccessAndReturnStatusAwaitingApprovalAndEstimatedAmountFull()
			throws Exception {
		PendingReturnRequest request = new PendingReturnRequest("RK-238", "carly@example.com");

		MvcResult pendingReturnResult = mvc.perform(post("/pending/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(request))
				).andExpect(status().isOk())
				.andExpect(jsonPath("token", isA(String.class)))
				.andReturn();

		PendingReturnResponse response =
				mapper.readValue(pendingReturnResult.getResponse().getContentAsString(), PendingReturnResponse.class);
		List<ItemRequest> itemRequests = new ArrayList<>();
		itemRequests.add(new ItemRequest("MU-5091", 2));
		itemRequests.add(new ItemRequest("NIKE-56", 1));
		CreateReturnRequest createRequest = new CreateReturnRequest(response.token(), itemRequests);

		MvcResult createReturnResult = mvc.perform(post("/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(createRequest))
				).andExpect(status().isOk())
				.andReturn();

		ReturnResponse createReturnResponse =
				mapper.readValue(createReturnResult.getResponse().getContentAsString(), ReturnResponse.class);

		mvc.perform(put("/returns/" + createReturnResponse.id() + "/items/" + "MU-5091" + "/qc/status")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(new UpdateQCStatus(ReturnItemStatus.ACCEPTED)))
		).andExpect(status().isOk());

		mvc.perform(get("/returns/" + createReturnResponse.id())
						.contentType(MediaType.APPLICATION_JSON)
				).andExpect(status().isOk())
				.andExpect(jsonPath("id", isA(String.class)))
				.andExpect(jsonPath("orderId", is("RK-238")))
				.andExpect(jsonPath("status", is(ReturnStatus.AWAITING_APPROVAL.name())))
				.andExpect(jsonPath("estimatedAmount", is("106.00")))
				.andExpect(jsonPath("$", not(hasProperty("refundAmount"))))
				.andExpect(jsonPath("items", hasSize(2)))
				.andExpect(jsonPath("items[0].sku", is("MU-5091")))
				.andExpect(jsonPath("items[0].status", is(ReturnItemStatus.ACCEPTED.name())))
		;
	}

	@Test
	public void givenValidRequestAndItemAcceptedAndRejected_whenPutQCStatus_thenReturnSuccessAndReturnStatusCompleteAndRefundAmountPartial()
			throws Exception {
		PendingReturnRequest request = new PendingReturnRequest("RK-238", "carly@example.com");

		MvcResult pendingReturnResult = mvc.perform(post("/pending/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(request))
				).andExpect(status().isOk())
				.andExpect(jsonPath("token", isA(String.class)))
				.andReturn();

		PendingReturnResponse response =
				mapper.readValue(pendingReturnResult.getResponse().getContentAsString(), PendingReturnResponse.class);
		List<ItemRequest> itemRequests = new ArrayList<>();
		itemRequests.add(new ItemRequest("MU-5091", 2));
		itemRequests.add(new ItemRequest("NIKE-56", 1));
		CreateReturnRequest createRequest = new CreateReturnRequest(response.token(), itemRequests);

		MvcResult createReturnResult = mvc.perform(post("/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(createRequest))
				).andExpect(status().isOk())
				.andReturn();

		ReturnResponse createReturnResponse =
				mapper.readValue(createReturnResult.getResponse().getContentAsString(), ReturnResponse.class);

		mvc.perform(put("/returns/" + createReturnResponse.id() + "/items/" + "MU-5091" + "/qc/status")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(new UpdateQCStatus(ReturnItemStatus.ACCEPTED)))
		).andExpect(status().isOk());

		mvc.perform(put("/returns/" + createReturnResponse.id() + "/items/" + "NIKE-56" + "/qc/status")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(new UpdateQCStatus(ReturnItemStatus.REJECTED)))
		).andExpect(status().isOk());

		mvc.perform(get("/returns/" + createReturnResponse.id())
						.contentType(MediaType.APPLICATION_JSON)
				).andExpect(status().isOk())
				.andExpect(jsonPath("id", isA(String.class)))
				.andExpect(jsonPath("orderId", is("RK-238")))
				.andExpect(jsonPath("status", is(ReturnStatus.COMPLETE.name())))
				.andExpect(jsonPath("refundAmount", is("30.50")))
				.andExpect(jsonPath("$", not(hasProperty("estimatedAmount"))))
				.andExpect(jsonPath("items", hasSize(2)))
				.andExpect(jsonPath("items[0].sku", is("MU-5091")))
				.andExpect(jsonPath("items[0].status", is(ReturnItemStatus.ACCEPTED.name())))
		;
	}

	@Test
	public void givenRequestItemStatusPending_whenPutQCStatus_thenReturnBadRequest()
			throws Exception {
		PendingReturnRequest request = new PendingReturnRequest("RK-238", "carly@example.com");

		MvcResult pendingReturnResult = mvc.perform(post("/pending/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(request))
				).andExpect(status().isOk())
				.andExpect(jsonPath("token", isA(String.class)))
				.andReturn();

		PendingReturnResponse response =
				mapper.readValue(pendingReturnResult.getResponse().getContentAsString(), PendingReturnResponse.class);
		List<ItemRequest> itemRequests = new ArrayList<>();
		itemRequests.add(new ItemRequest("MU-5091", 2));
		CreateReturnRequest createRequest = new CreateReturnRequest(response.token(), itemRequests);

		MvcResult createReturnResult = mvc.perform(post("/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(createRequest))
				).andExpect(status().isOk())
				.andReturn();

		ReturnResponse createReturnResponse =
				mapper.readValue(createReturnResult.getResponse().getContentAsString(), ReturnResponse.class);

		mvc.perform(put("/returns/" + createReturnResponse.id() + "/items/" + "MU-5091" + "/qc/status")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(new UpdateQCStatus(ReturnItemStatus.PENDING)))
		).andExpect(status().isBadRequest());

	}

	@Test
	public void givenInvalidReturnId_whenPutQCStatus_thenReturnNotFound()
			throws Exception {
		mvc.perform(put("/returns/" + "INVALID" + "/items/" + "MU-5091" + "/qc/status")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(new UpdateQCStatus(ReturnItemStatus.ACCEPTED)))
		).andExpect(status().isNotFound());

	}

	@Test
	public void givenRInvalidSku_whenPutQCStatus_thenReturnNotFound()
			throws Exception {
		PendingReturnRequest request = new PendingReturnRequest("RK-238", "carly@example.com");

		MvcResult pendingReturnResult = mvc.perform(post("/pending/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(request))
				).andExpect(status().isOk())
				.andExpect(jsonPath("token", isA(String.class)))
				.andReturn();

		PendingReturnResponse response =
				mapper.readValue(pendingReturnResult.getResponse().getContentAsString(), PendingReturnResponse.class);
		List<ItemRequest> itemRequests = new ArrayList<>();
		itemRequests.add(new ItemRequest("MU-5091", 2));
		CreateReturnRequest createRequest = new CreateReturnRequest(response.token(), itemRequests);

		MvcResult createReturnResult = mvc.perform(post("/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(createRequest))
				).andExpect(status().isOk())
				.andReturn();

		ReturnResponse createReturnResponse =
				mapper.readValue(createReturnResult.getResponse().getContentAsString(), ReturnResponse.class);

		mvc.perform(put("/returns/" + createReturnResponse.id() + "/items/" + "INVALID" + "/qc/status")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(new UpdateQCStatus(ReturnItemStatus.ACCEPTED)))
		).andExpect(status().isNotFound());

	}

}
