package co.returnkey.test.controller;

import co.returnkey.test.model.PendingReturnRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.isA;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
public class PendingReturnControllerTests {
	@Autowired
	private MockMvc mvc;
	private ObjectMapper mapper = new ObjectMapper();

	@Test
	public void givenValidRequest_whenPostPendingReturn_thenReturnSuccess() throws Exception {
		PendingReturnRequest request = new PendingReturnRequest("RK-238", "carly@example.com");
		mvc.perform(post("/pending/returns")
						.contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(request))
				).andExpect(status().isOk())
				.andExpect(jsonPath("token", isA(String.class)));
	}

	@Test
	public void givenInvalidOrderId_whenPostPendingReturn_thenReturnNotFound() throws Exception {
		PendingReturnRequest request = new PendingReturnRequest("RY-238", "carly@example.com");
		mvc.perform(post("/pending/returns")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(request))
		).andExpect(status().isNotFound());
	}

	@Test
	public void givenInvalidEmail_whenPostPendingReturn_thenReturnNotFound() throws Exception {
		PendingReturnRequest request = new PendingReturnRequest("RK-238", "carlo@example.com");
		mvc.perform(post("/pending/returns")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(request))
		).andExpect(status().isNotFound());
	}

	@Test
	public void givenEmptyOrderId_whenPostPendingReturn_thenReturnBadRequest() throws Exception {
		PendingReturnRequest request = new PendingReturnRequest("", "carly@example.com");
		mvc.perform(post("/pending/returns")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(request))
		).andExpect(status().isBadRequest());
	}

	@Test
	public void givenEmptyEmail_whenPostPendingReturn_thenReturnBadRequest() throws Exception {
		PendingReturnRequest request = new PendingReturnRequest("RK-238", "");
		mvc.perform(post("/pending/returns")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(request))
		).andExpect(status().isBadRequest());
	}

	@Test
	public void givenNullOrderId_whenPostPendingReturn_thenReturnBadRequest() throws Exception {
		PendingReturnRequest request = new PendingReturnRequest(null, "carly@example.com");
		mvc.perform(post("/pending/returns")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(request))
		).andExpect(status().isBadRequest());
	}

	@Test
	public void givenNullEmail_whenPostPendingReturn_thenReturnBadRequest() throws Exception {
		PendingReturnRequest request = new PendingReturnRequest("RK-238", null);
		mvc.perform(post("/pending/returns")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(request))
		).andExpect(status().isBadRequest());
	}
}
